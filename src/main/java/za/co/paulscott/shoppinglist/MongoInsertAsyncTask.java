package za.co.paulscott.shoppinglist;

import android.os.AsyncTask;

public class MongoInsertAsyncTask extends
		AsyncTask<ListItem, Integer, String> {

	@Override
	protected String doInBackground(ListItem... params) {
		// Look up the details
		MongoConnector conn = new MongoConnector();
		for (ListItem param : params) {
			conn.insertListItem(param);
			return null;
		}
		return null;
	}

}