package za.co.paulscott.shoppinglist;

import org.json.JSONObject;

import android.os.AsyncTask;

import com.mongodb.DBObject;

public class MongoAsyncTask extends AsyncTask<String, Integer, JSONObject> {

	@Override
	protected JSONObject doInBackground(String... params) {
		// Look up the details
		MongoConnector conn = new MongoConnector();
		for (String param : params) {
			DBObject doc = conn.findByBarcode(param);
			if (doc != null) {
				JSONParser jParser = new JSONParser();
				JSONObject json = jParser.getJSONFromString(doc);
				return json;
			}

		}
		return null;
	}

}
