package za.co.paulscott.shoppinglist;

import com.bugsense.trace.BugSenseHandler;

import android.app.Application;

public class TheApplication extends Application {

    /*
     * (non-Javadoc)
     * @see android.app.Application#onCreate()
     */
    @Override
    public void onCreate() {
        super.onCreate();
        BugSenseHandler.initAndStartSession( this, "9790585d" );
    }

}
