package za.co.paulscott.shoppinglist;

import java.util.List;

import za.co.paulscott.shoppinglist.R;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ScanActivity extends Activity {

	private static final String TAG = "ScanActivity";
	TextView tvStatus;
	TextView tvResult;

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            If the activity is being re-initialized after previously being
	 *            shut down then this Bundle contains the data it most recently
	 *            supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it
	 *            is null.</b>
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// BugSenseHandler.initAndStartSession(HelloAndroidActivity.this,
		// "9790585d");
		setContentView(R.layout.activity_scan);

		Button scanBtn = (Button) findViewById(R.id.btnScan);
		scanBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				final boolean scanAvailable = isIntentAvailable(
						getApplicationContext(),
						"com.google.zxing.client.android.SCAN");
				if (scanAvailable) {
					try {
						Intent intent = new Intent(
								"com.google.zxing.client.android.SCAN");
						intent.putExtra("SCAN_MODE",
								"QR_CODE_MODE,PRODUCT_MODE");
						intent.putExtra("SAVE_HISTORY", false);
						startActivityForResult(intent, 0);
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(getApplicationContext(), "ERROR:" + e,
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Please install the ZXing scanner app",
							Toast.LENGTH_SHORT).show();
					Uri uri = Uri
							.parse("http://play.google.com/store/apps/details?id=com.google.zxing.client.android");
					Intent intent = new Intent("android.intent.action.VIEW");
					intent.setData(uri);
					startActivity(intent);
				}
			}

		});

		Button entryBtn = (Button) findViewById(R.id.btnEntry);
		entryBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try {

					Toast.makeText(getApplicationContext(), "Entry time!",
							Toast.LENGTH_SHORT).show();
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), "ERROR:" + e,
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				Intent returnIntent = new Intent();
				returnIntent.putExtra("result",
						intent.getStringExtra("SCAN_RESULT"));
				setResult(RESULT_OK, returnIntent);
				finish();
			} else if (resultCode == RESULT_CANCELED) {
				tvStatus.setText("Press a button to start a scan.");
				tvResult.setText("Scan cancelled.");
			}
		}
	}

	/**
	 * Indicates whether the specified action can be used as an intent. This
	 * method queries the package manager for installed packages that can
	 * respond to an intent with the specified action. If no suitable package is
	 * found, this method returns false.
	 * 
	 * @param context
	 *            The application's environment.
	 * @param action
	 *            The Intent action to check for availability.
	 * 
	 * @return True if an Intent with the specified action can be sent and
	 *         responded to, false otherwise.
	 */
	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

}
