/**
 * 
 */
package za.co.paulscott.shoppinglist;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author paul
 * 
 */
public class ShoppingListOverviewActivity extends Activity implements
		LoaderManager.LoaderCallbacks<Cursor> {

	private static final int DELETE_ID = Menu.FIRST + 1;
	private static final String TAG = "ShoppingListOverview";
	// private Cursor cursor;
	private SimpleCursorAdapter adapter;

	private ListView mList;
	private TextView mAddButton, mScanButton;

	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mList = (ListView) findViewById(android.R.id.list);
		mAddButton = (TextView) findViewById(R.id.btn_add_items);
		mScanButton = (TextView) findViewById(R.id.btn_scan);
		registerForContextMenu(mList);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		updateList();
		mAddButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				final Builder bldr = new Builder(
						ShoppingListOverviewActivity.this);
				bldr.setTitle(R.string.product_add);
				final View layout = LayoutInflater.from(
						ShoppingListOverviewActivity.this).inflate(
						R.layout.dialog_add_user_item, null);
				// final EditText barcode = (EditText) layout
				// .findViewById(R.id.barcode_number);
				// barcode.setText("");
				// barcode.setEnabled(false);
				final EditText productName = (EditText) layout
						.findViewById(R.id.name);
				final EditText productDescription = (EditText) layout
						.findViewById(R.id.description);
				final EditText quantity = (EditText) layout
						.findViewById(R.id.quantity);
				final EditText price = (EditText) layout
						.findViewById(R.id.price);
				bldr.setView(layout);
				bldr.setNegativeButton(android.R.string.cancel, null);
				bldr.setPositiveButton(R.string.add,
						new DialogInterface.OnClickListener() {

							private Float theprice;

							public void onClick(DialogInterface dialog,
									int which) {
								ContentValues values = new ContentValues();
								// values.put(DatabaseHandler.KEY_BARCODE,
								// barcode.getText().toString());
								if (!price.getText().toString().equals("")) {
									theprice = Float.valueOf(price.getText().toString());
								} else {
									theprice = Float.valueOf("0.00");
								}
								values.put(DatabaseHandler.KEY_QUANTITY,
										quantity.getText().toString());
								values.put(DatabaseHandler.KEY_DESCRIPTION,
										productDescription.getText().toString());
								values.put(DatabaseHandler.KEY_NAME,
										productName.getText().toString());
								values.put(DatabaseHandler.KEY_PRICE, theprice);
								
								getContentResolver()
										.insert(ShoppingListContentProvider.CONTENT_URI,
												values);

								ListItem item = new ListItem(
										"",
										productName.getText().toString(),
										productDescription.getText().toString(),
										theprice, Integer
												.parseInt(quantity.getText()
														.toString()), 0);

								// new MongoInsertAsyncTask()
								// .execute(item);

							}
						});
				final Dialog dlg = bldr.create();
				dlg.setCancelable(true);
				dlg.setCanceledOnTouchOutside(false);
				dlg.show();

			}
		});
		mScanButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				scanBarcode();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	// create the menu based on the XML defintion
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		// inflater.inflate(R.menu.listmenu, menu);
		return true;
	}

	// Reaction to the menu selection
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.insert:
			createShoppingList();
			return true;
		case R.id.scan:
			scanBarcode();
			return true;
		case R.id.about:
			aboutShoppingList();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	private void aboutShoppingList() {
		Toast.makeText(this,
				"Shopping list application by Paul Scott and Mitch Wong Ho",
				Toast.LENGTH_LONG).show();

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case DELETE_ID:
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			Uri uri = Uri.parse(ShoppingListContentProvider.CONTENT_URI + "/"
					+ info.id);
			getContentResolver().delete(uri, null, null);
			updateList();
			return true;
		}
		return super.onContextItemSelected(item);
	}

	private void createShoppingList() {
		Intent i = new Intent(this, ShoppingListDetailActivity.class);
		startActivity(i);
	}

	private void scanBarcode() {
		final boolean scanAvailable = isIntentAvailable(
				getApplicationContext(), "com.google.zxing.client.android.SCAN");
		if (scanAvailable) {
			try {
				Intent intent = new Intent(
						"com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
				intent.putExtra("SAVE_HISTORY", false);
				startActivityForResult(intent, 0);
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "ERROR:" + e,
						Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getApplicationContext(),
					"Please install the ZXing scanner app", Toast.LENGTH_SHORT)
					.show();
			Uri uri = Uri
					.parse("http://play.google.com/store/apps/details?id=com.google.zxing.client.android");
			Intent intent = new Intent("android.intent.action.VIEW");
			intent.setData(uri);
			startActivity(intent);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String result = intent.getStringExtra("SCAN_RESULT");
				AsyncTask<String, Integer, JSONObject> data = new MongoAsyncTask()
						.execute(result);
				try {
					JSONObject json = data.get();
					if (json != null) {
						String desc = json.getString("description");
						// add to the database
						ContentValues values = new ContentValues();
						values.put(DatabaseHandler.KEY_BARCODE, result);
						values.put(DatabaseHandler.KEY_QUANTITY, 1);
						values.put(DatabaseHandler.KEY_DESCRIPTION, desc);
						values.put(DatabaseHandler.KEY_PRICE,
								Float.valueOf("0.00"));
						getContentResolver()
								.insert(ShoppingListContentProvider.CONTENT_URI,
										values);
					} else {
						final Builder bldr = new Builder(this);
						bldr.setTitle(R.string.product_not_found);
						final View layout = LayoutInflater.from(this).inflate(
								R.layout.dialog_add_item, null);
						final EditText barcode = (EditText) layout
								.findViewById(R.id.barcode_number);
						barcode.setText(result);
						barcode.setEnabled(false);
						final EditText productName = (EditText) layout
								.findViewById(R.id.name);
						final EditText productDescription = (EditText) layout
								.findViewById(R.id.description);
						final EditText quantity = (EditText) layout
								.findViewById(R.id.quantity);
						final EditText price = (EditText) layout
								.findViewById(R.id.price);
						bldr.setView(layout);
						bldr.setNegativeButton(android.R.string.cancel, null);
						bldr.setPositiveButton(R.string.add,
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int which) {
										ContentValues values = new ContentValues();
										values.put(DatabaseHandler.KEY_BARCODE,
												barcode.getText().toString());
										values.put(
												DatabaseHandler.KEY_QUANTITY,
												quantity.getText().toString());
										values.put(
												DatabaseHandler.KEY_DESCRIPTION,
												productDescription.getText()
														.toString());
										if (!price.getText().toString()
												.equals("")) {
											values.put(
													DatabaseHandler.KEY_PRICE,
													Float.valueOf(price
															.getText()
															.toString()));
										}
										values.put(DatabaseHandler.KEY_NAME,
												productName.getText().toString());
										getContentResolver()
												.insert(ShoppingListContentProvider.CONTENT_URI,
														values);

										ListItem item = new ListItem(barcode
												.getText().toString(),
												productName.getText()
														.toString(),
												productDescription.getText()
														.toString(), Float
														.valueOf("0.00"),
												Integer.parseInt(quantity
														.getText().toString()),
												0);

										new MongoInsertAsyncTask()
												.execute(item);

									}
								});
						final Dialog dlg = bldr.create();
						dlg.setCancelable(true);
						dlg.setCanceledOnTouchOutside(false);
						dlg.show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, "Scan Cancelled!", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	// Opens the second activity if an entry is clicked
	// @Override
	// protected void onListItemClick(ListView l, View v, int position, long id)
	// {
	// super.onListItemClick(l, v, position, id);
	// Intent i = new Intent(this, ShoppingListDetailActivity.class);
	// Uri shoppingListUri = Uri.parse(ShoppingListContentProvider.CONTENT_URI
	// + "/" + id);
	// i.putExtra(ShoppingListContentProvider.CONTENT_ITEM_TYPE,
	// shoppingListUri);
	// startActivity(i);
	// }
	//
	private void updateList() {

		// Fields from the database (projection)
		// Must include the _id column for the adapter to work
		String[] from = new String[] { DatabaseHandler.KEY_DESCRIPTION };
		// Fields on the UI to which we map
		int[] to = new int[] { R.id.label };

		getLoaderManager().initLoader(0, null, this);
		adapter = new SimpleCursorAdapter(this, R.layout.list_row, null, from,
				to, 0);

		mList.setAdapter(adapter);
		// //
		final View emptyListView = findViewById(R.id.emptylist);
		mList.setEmptyView(emptyListView);
		final TextView emptylistMessage = (TextView) emptyListView
				.findViewById(R.id.emptylist_message);
		final TextView hintToAdd = (TextView) emptyListView
				.findViewById(R.id.hint_to_add);
		final TextView hintToScan = (TextView) emptyListView
				.findViewById(R.id.hint_to_scan);
		final Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/idolwild.ttf");
		if (tf != null) {
			emptylistMessage.setTypeface(tf);
			hintToAdd.setTypeface(tf);
			hintToScan.setTypeface(tf);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, DELETE_ID, 0, R.string.menu_delete);
	}

	// creates a new loader after the initLoader () call
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String[] projection = { DatabaseHandler.KEY_ID,
				DatabaseHandler.KEY_DESCRIPTION };
		CursorLoader cursorLoader = new CursorLoader(this,
				ShoppingListContentProvider.CONTENT_URI, projection, null,
				null, null);
		return cursorLoader;
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		adapter.swapCursor(data);
	}

	public void onLoaderReset(Loader<Cursor> loader) {
		// data is not available anymore, delete reference
		adapter.swapCursor(null);
	}

	/**
	 * Indicates whether the specified action can be used as an intent. This
	 * method queries the package manager for installed packages that can
	 * respond to an intent with the specified action. If no suitable package is
	 * found, this method returns false.
	 * 
	 * @param context
	 *            The application's environment.
	 * @param action
	 *            The Intent action to check for availability.
	 * 
	 * @return True if an Intent with the specified action can be sent and
	 *         responded to, false otherwise.
	 */
	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

}
