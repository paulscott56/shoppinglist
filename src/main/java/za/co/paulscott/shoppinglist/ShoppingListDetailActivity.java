package za.co.paulscott.shoppinglist;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ShoppingListDetailActivity extends Activity {

	/*
	 * ShoppingListDetailActivity allows to enter a new item or to change an
	 * existing
	 */

	private static final String TAG = "ShoppingListDetails";
	private EditText mProduct;
	private String mBarcode;

	private Uri shoppingListURI;

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.list_edit);

		mBarcode = getIntent().getStringExtra("barcode");
		mProduct = (EditText) findViewById(R.id.todo_edit_description);
		Button confirmButton = (Button) findViewById(R.id.todo_edit_button);

		Bundle extras = getIntent().getExtras();
		
		// check from the saved Instance
		shoppingListURI = (bundle == null) ? null : (Uri) bundle
				.getParcelable(ShoppingListContentProvider.CONTENT_ITEM_TYPE);

		// Or passed from the other activity
		if (extras != null) {
			shoppingListURI = extras
					.getParcelable(ShoppingListContentProvider.CONTENT_ITEM_TYPE);

			fillData(shoppingListURI);
		}

		confirmButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				if (TextUtils.isEmpty(mProduct.getText().toString())) {
					makeToast();
				} else {
					setResult(RESULT_OK);
					finish();
				}
			}

		});
	}

	private void fillData(Uri uri) {
		String[] projection = { DatabaseHandler.KEY_DESCRIPTION,
				DatabaseHandler.KEY_DESCRIPTION, DatabaseHandler.KEY_BARCODE };
		Cursor cursor = getContentResolver().query(uri, projection, null, null,
				null);
		if (cursor != null) {
			cursor.moveToFirst();

			mProduct.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(DatabaseHandler.KEY_DESCRIPTION)));
//			mBarcode.setText(cursor.getString(cursor
//					.getColumnIndexOrThrow(DatabaseHandler.KEY_BARCODE)));

			// always close the cursor
			cursor.close();
		}
	}

	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveState();
		outState.putParcelable(ShoppingListContentProvider.CONTENT_ITEM_TYPE, shoppingListURI);
	}

	@Override
	protected void onPause() {
		super.onPause();
		saveState();
	}

	private void saveState() {
		String barcode = mBarcode;
		String description = mProduct.getText().toString();

		if (description.length() == 0) {
			return;
		}

		ContentValues values = new ContentValues();
		values.put(DatabaseHandler.KEY_BARCODE, barcode);
		values.put(DatabaseHandler.KEY_DESCRIPTION, description);

		if (shoppingListURI == null) {
			// New 
			shoppingListURI = getContentResolver().insert(
					ShoppingListContentProvider.CONTENT_URI, values);
		} else {
			// Update 
			getContentResolver().update(shoppingListURI, values, null, null);
		}
		
		// send the barcode to Mongo Service, if null, then add it (it should be null if we are here...)
		ListItem item = new ListItem(barcode, "", description, Float.valueOf("0.00"), 1, 0);
		Log.i(TAG, item.toString());
		AsyncTask<ListItem, Integer, String> mongo = new MongoInsertAsyncTask().execute(item);
		
	}

	private void makeToast() {
		Toast.makeText(ShoppingListDetailActivity.this,
				"Please add a description", Toast.LENGTH_LONG).show();
	}
}
