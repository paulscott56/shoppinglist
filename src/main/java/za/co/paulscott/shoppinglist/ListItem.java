package za.co.paulscott.shoppinglist;

public class ListItem {
	private int id;
	private String barcode;
	private String description;
	private float price;
	private int quantity;
	private int selected = 0;
	private String name;
	
	public ListItem() {
		
	}

	public ListItem(int id, String barcode, String name, String description, float price,
			int quantity, int selected) {
		super();
		this.id = id;
		this.barcode = barcode;
		this.description = description;
		this.price = price;
		this.quantity = quantity;
		this.selected = selected;
	}
	
	

	public ListItem(String barcode, String name, String description, float price,
			int quantity, int checked) {
		super();
		this.barcode = barcode;
		this.description = description;
		this.price = price;
		this.quantity = quantity;
		this.selected = checked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int isSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}