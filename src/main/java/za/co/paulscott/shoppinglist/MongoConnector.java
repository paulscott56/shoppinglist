package za.co.paulscott.shoppinglist;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

public class MongoConnector {
	private static final String TAG = "MongoConnector";
	private boolean auth;
	private DB db;
	private DBCollection collection;

	public MongoConnector() {
		doAuth();
	}

	private void doAuth() {
		try {
			MongoClient mongoClient = new MongoClient("ds027708.mongolab.com",
					27708);
			db = mongoClient.getDB("products");
			auth = db.authenticate("shoppinglistapp",
					"TheBlueChairNo5".toCharArray());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}

	}

	public DBObject findByBarcode(String barcode) {
		if (auth) {
			try {
				collection = db.getCollection("barcodes");
				BasicDBObject query = new BasicDBObject("barcode", barcode);
				DBObject doc = collection.findOne(query);
				return doc;
			} catch (MongoException e) {
				e.printStackTrace();
			}
		} else {
			doAuth();
			System.out.println("Something went horribly wrong!");
		}
		return null;
	}

	public List<DBObject> findByProduct(String product) {
		if (auth) {
			try {
				List<DBObject> docs = new ArrayList<DBObject>();
				collection = db.getCollection("barcodes");
				BasicDBObject query = new BasicDBObject("product", product);
				DBCursor cursor = collection.find(query);
				try {
					while (cursor.hasNext()) {
						DBObject doc = cursor.next();
						docs.add(doc);
					}
					return docs;
				} finally {
					cursor.close();
				}
			} catch (MongoException e) {
				e.printStackTrace();
			}
		} else {
			doAuth();
			System.out.println("Something went horribly wrong!");
		}
		return null;
	}

	public void insertListItem(ListItem item) {
		collection = db.getCollection("barcodes");
		BasicDBObject doc = new BasicDBObject("barcode", item.getBarcode())
				.append("description", item.getDescription())
				.append("price", item.getPrice())
				.append("quantity", item.getQuantity())
				.append("selected", item.isSelected());
		Log.i(TAG, doc.toString());
		collection.insert(doc);
	}
	
	public long getCollectionCount() {
		return collection.count();
	}
}
