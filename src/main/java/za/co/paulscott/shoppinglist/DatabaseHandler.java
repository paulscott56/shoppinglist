package za.co.paulscott.shoppinglist;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "shoppingList";
	public static final String TABLE_LIST = "listitems";
	public static final String KEY_ID = "_id";
	public static final String KEY_BARCODE = "barcode";
	public static final String KEY_NAME = "name";
	public static final String KEY_DESCRIPTION = "description";
	public static final String KEY_PRICE = "price";
	public static final String KEY_QUANTITY = "quantity";
	public static final String KEY_CHECKED = "checked";
	public static final String TAG = "DBHandler";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LISTITEMS_TABLE = "CREATE TABLE " + TABLE_LIST + " (" + KEY_ID
				+ " INTEGER PRIMARY KEY, " + KEY_BARCODE + " TEXT,"
				+ KEY_NAME + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_PRICE + " FLOAT, "
				+ KEY_QUANTITY + " INTEGER," + KEY_CHECKED + " BOOLEAN)";
		db.execSQL(CREATE_LISTITEMS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);
		// Create tables again
		onCreate(db);
	}

	/**
	 * CRUD methods
	 */
	public void addItem(ListItem item) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_BARCODE, item.getBarcode());
		values.put(KEY_NAME, item.getName());
		values.put(KEY_DESCRIPTION, item.getDescription());
		values.put(KEY_PRICE, item.getPrice());
		values.put(KEY_QUANTITY, item.getQuantity());
		values.put(KEY_CHECKED, item.isSelected());

		db.insert(TABLE_LIST, null, values);
		db.close();
	}

	public ListItem getItem(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_LIST, new String[] { KEY_ID,
				KEY_BARCODE, KEY_DESCRIPTION, KEY_NAME, KEY_PRICE, KEY_QUANTITY, KEY_CHECKED }, KEY_ID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null,
				null);
		if (cursor != null)
			cursor.moveToFirst();

		ListItem item = new ListItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
				cursor.getString(3), cursor.getFloat(4), cursor.getInt(5), cursor.getInt(6));
		// return item
		return item;
	}
	
	public ListItem getItemByBarcode(String barcode) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_LIST, new String[] { KEY_ID,
				KEY_BARCODE, KEY_NAME, KEY_DESCRIPTION, KEY_PRICE, KEY_QUANTITY, KEY_CHECKED }, KEY_BARCODE
				+ "=?", new String[] { barcode }, null, null, null,
				null);
		if (cursor != null)
			cursor.moveToFirst();

		ListItem item = new ListItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
				cursor.getString(3), cursor.getFloat(4), cursor.getInt(5), cursor.getInt(6));
		// return item
		return item;
	}

	public ArrayList<ListItem> getAllItems() {
		ArrayList<ListItem> itemList = new ArrayList<ListItem>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + TABLE_LIST;
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	            ListItem item = new ListItem();
	            item.setId(Integer.parseInt(cursor.getString(0)));
	            item.setBarcode(cursor.getString(1));
	            item.setName(cursor.getString(2));
	            item.setDescription(cursor.getString(3));
	            item.setPrice(cursor.getFloat(4));
	            item.setQuantity(cursor.getInt(5));
	            item.setSelected(cursor.getInt(6));
	            // Adding item to list
	            itemList.add(item);
	        } while (cursor.moveToNext());
	    }
	 
	    // return item list
	    return itemList;
	}

	public int getListCount() {
		String countQuery = "SELECT  * FROM " + TABLE_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        //cursor.close();
 
        // return count
        return cursor.getCount();
	}

	public int updateItem(ListItem item) {
		SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_BARCODE, item.getBarcode());
	    values.put(KEY_NAME, item.getName());
	    values.put(KEY_DESCRIPTION, item.getDescription());
	    values.put(KEY_PRICE, item.getPrice());
	    values.put(KEY_QUANTITY, item.getQuantity());
	    values.put(KEY_CHECKED, item.isSelected());
	
	    // updating row
	    return db.update(TABLE_LIST, values, KEY_ID + " = ?",
	            new String[] { String.valueOf(item.getId()) });

	}

	public void deleteItem(int i) {
		Log.i(TAG, "Deleting " + i);
		SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(TABLE_LIST, KEY_ID + " = ?",
	            new String[] { String.valueOf(i) });
	    db.close();
	}
	
	public void deleteCheckedItems() {
		ArrayList<ListItem> allitems = getAllItems();
		for (ListItem item : allitems) {
	    	int checked = item.isSelected();
	    	if(checked > 0) {
	    		deleteItem(item.getId());
	    	}
	    }
	}

}
